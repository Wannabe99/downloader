@echo off
color 1f
cls

echo.
echo.    ������������������
echo.  ����������������������
echo.  �������������������۲�                  ��    ��  ��    ��  ������
echo.  ��  ��  ��  ��  ��  ��                  ��    ��  ��    ��  ��    ��
echo.  ��  ��  ��  ��  ��  ��                  ��    ��  ��    ��  ������
echo.  ��  ��  ��  ��  ��������                ��    ��  ��    ��  ��
echo.  ��  ��  ��  ���۰���������                ����      ����    ��
echo.  ��  ��  ��  �۰������۰�����
echo.  ��  ��  ���۰��������۰�������
echo.  ��  ��  ���۰��������۰�������    ������    ��    ��  ��      ��  ������
echo.  ��  ��  ���۰������������۰���    ��    ��  ��    ��  ����  ����  ��    ��
echo.  ��  ��  ���۰����������۲�����    ��    ��  ��    ��  ��  ��  ��  ������
echo.  ��  ��  ���۰��������۲�������    ��    ��  ��    ��  ��      ��  ��
echo.  �������������۰�������������      ������      ����    ��      ��  ��
echo.  ���������������۰���������
echo.    ����������������������
echo.
echo.  Starting the UUP dump downloader application...

REG QUERY HKU\S-1-5-19\Environment >NUL 2>&1
IF %ERRORLEVEL% EQU 0 goto :HAS_ADMIN
echo.  Requesting administrative privileges...

set "command=""%~f0"" %*"
set "command=%command:"=\"%"

start /WAIT "" "%~dp0files\AutoHotkey.exe" "%~dp0files\run.ahk" 3 cmd.exe /c %command%
IF %ERRORLEVEL% EQU 0 (
    echo.  Successfully obtained administrative privileges!
) ELSE (
    echo.  Failed to obtain administrative privileges!
)

goto :QUIT

:HAS_ADMIN
set "UUPDUMP_DEFAULTPATH=%~dp0"
start "" "%~dp0files\AutoHotkey.exe" "%~dp0files\uupdownloader.ahk" %*
color

:QUIT
color
exit /b
